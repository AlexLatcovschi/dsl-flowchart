// Predefine some matches
const mutatorStart = "(?:\\b|^)";
const mutatorEnd = "(?:\\b|$)";
const typeStart = "(?:[|#]|\\b|^)";
const typeEnd = "(?:\\||\\b|$)";
const stringAnchor = "['\"`]";
const staticValueStart = mutatorStart;
const staticValueEnd = mutatorEnd;
// Define language tokens
const grammar = [
    // Comments
    {
        id: "console_log_start",
        match: ">"
    },
    {
        id: "console_log_end",
        match: "<"
    },
    // basically we don`t use this types but should be accessible as in any language
    {
        id: "integer",
        match: `${staticValueStart}(?:0d)?-?(?:[1-9]\\d*|0)(?:e-?[1-9]\\d*)?${staticValueEnd}`
    },
    {
        id: "float",
        match: `${staticValueStart}(?:0d)?(?:(?:-?\\d+)|\\.)(?:(?:\\.\\d+)|\\d+)e?(?:-?\\d+)${staticValueEnd}`
    },
    {
        id: "binary",
        match: `${staticValueStart}0b[01]+${staticValueEnd}`
    },
    {
        id: "octal",
        match: `${staticValueStart}0o[0-7]+${staticValueEnd}`
    },
    {
        id: "hex",
        match: `${staticValueStart}0o[0-9A-Fa-f]+${staticValueEnd}`
    },
    {
        id: "bool",
        match: `${staticValueStart}(?:true|false)${staticValueEnd}`
    },
    {
        id: "char",
        match: `(?<=(?:${staticValueStart}${stringAnchor})|[\\x00-\\x7F])[\\x00-\\x7F](?=(?:[\\x00-\\x7F]+)?${stringAnchor}${staticValueEnd})`
    },
    {
        id: "uchar",
        match: `(?<=(?:${staticValueStart}${stringAnchor})|[\\u{0}-\\u{FFFFF}])[\\u{0}-\\u{FFFFF}](?=(?:[\\u{0}-\\u{FFFFF}]+)?${stringAnchor}${staticValueEnd})`
    },
    // Modifiers
    {
        id: "function_name_directive",
        match: "@"
    },
    {
        id: "context_modifier",
        match: "\\.\\."
    },
    {
        id: "multiplication_modifier",
        match: "\\*"
    },
    {
        id: "equality_modifier",
        match: "=="
    },
    {
        id: "type_modifier",
        match: "#"
    },
    {
        id: "access_modifier",
        match: "\\."
    },
    {
        id: "alternative_modifier",
        match: "\\|"
    },
    {
        id: "bind_modifier",
        match: "=>"
    },
    {
        id: "escape_modifier",
        match: "\\$"
    },
    {
        id: "addition_modifier",
        match: "\\+"
    },
    {
        id: "increasement_modifier",
        match: "\\+\\+"
    },
    {
        id: "substitution_modifier",
        match: "-"
    },
    {
        id: "decreasement_modifier",
        match: "--"
    },
    {
        id: "subdivision_modifier",
        match: "\\/"
    },
    {
        id: "power_modifier",
        match: "\\^"
    },
    {
        id: "modulo_modifier",
        match: "%"
    },
    {
        id: "negation_modifier",
        match: "!"
    },
    {
        id: "assignment_modifier",
        match: "="
    },
    {
        id: "join_modifier",
        match: "&"
    },
    {
        id: "question_modifier",
        match: "\\?"
    },
    //Types
    {
        id: "bool_type",
        match: `${typeStart}bool${typeEnd}`
    },
    {
        id: "char_type",
        match: `${typeStart}char${typeEnd}`
    },
    {
        id: "uchar_type",
        match: `${typeStart}uchar${typeEnd}`
    },
    {
        id: "string_type",
        match: `${typeStart}string${typeEnd}`
    },
    // Anchors and brackets
    {
        id: "string_anchor",
        match: stringAnchor
    },
    {
        id: "curly_brackets_begin",
        match: "\\{"
    },
    {
        id: "curly_brackets_end",
        match: "\\{"
    },
    {
        id: "comma",
        match: ","
    },
    {
        id: "square_brackets_begin",
        match: "\\["
    },
    {
        id: "square_brackets_end",
        match: "\\]"
    },
    {
        id: "round_brackets_begin",
        match: "\\("
    },
    {
        id: "round_brackets_end",
        match: "\\)"
    },
    {
        id: "angle_brackets_begin",
        match: "<"
    },
    {
        id: "angle_brackets_end",
        match: ">"
    },
];
