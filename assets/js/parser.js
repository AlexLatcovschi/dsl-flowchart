// read grammar from grammar.js as grammar variable

function parseString(str) {
    logInConsole(getSubstring(str, findSymbol("console_log_start").match, findSymbol("console_log_end").match));

    str = str.split(" ");
    str.map(el => {
        initDSLFunctions(getSubstring(el, findSymbol("function_name_directive").match, ''));
    });
    str = '';
    return str;
}

function getSubstring(str, el1, el2) {
    return str.substring(
        str.lastIndexOf(el1) + 1,
        str.lastIndexOf(el2)
    );
}


function initDSLFunctions(func) {
    switch (func) {
        case "saveJson" :
            saveJson();
            return;
        case "loadChart" :
            loadChart();
            return;
        case "printDiagram" :
            printDiagram();
            return;
    }
}

function logInConsole(str) {
    console.log(str);
}

function findSymbol(id) {
    let returnEl = {};
    grammar.map(el => {
        if (el.id === id) {
            returnEl = el;
        }
    });
    return returnEl;
}

// Show the diagram's model in JSON format that the user may edit
function saveJson() {
    document.getElementById("mySavedModel").value = myDiagram.model.toJson();
    myDiagram.isModified = false;
}

function loadChart() {
    myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
}

// print the diagram by opening a new window holding SVG images of the diagram contents for each page
function printDiagram() {
    var svgWindow = window.open();
    if (!svgWindow) return;  // failure to open a new Window
    var printSize = new go.Size(700, 960);
    var bnds = myDiagram.documentBounds;
    var x = bnds.x;
    var y = bnds.y;
    while (y < bnds.bottom) {
        while (x < bnds.right) {
            var svg = myDiagram.makeSVG({scale: 1.0, position: new go.Point(x, y), size: printSize});
            svgWindow.document.body.appendChild(svg);
            x += printSize.width;
        }
        x = bnds.x;
        y += printSize.height;
    }
    setTimeout(function () {
        svgWindow.print();
    }, 1);
}
